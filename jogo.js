var jogador = 0;
const jogadores = [
	{nome_do_jogador: 'Cheryl'},
	{nome_do_jogador: 'Veronica'}
  ];
var jogadas = 0;
var vencedor = false;

// para saber de quem é a vez de jogar
function jogadorDaVez(jogador) {
	let quemJoga = document.getElementById('jogador');
  
	if( jogador === 0 ) {
	  quemJoga.textContent = jogadores[1].nome_do_jogador;
	  quemJoga.className = jogadores[1].nome_do_jogador;
  
	  return 1;
	} else {
	  quemJoga.textContent = jogadores[0].nome_do_jogador;
	  quemJoga.className = jogadores[0].nome_do_jogador;
  
	  return 0;
	};
  };
  
// para checar se alguém venceu
function checarVitoria(clickedIn) {
	const possibilidades = {
	  '0': [[0, 1, 2], [0, 4, 8], [0, 3, 6]],
	  '1': [[0, 1, 2], [1, 4, 7]],
	  '2': [[0, 1, 2], [6, 4, 2], [2, 5, 8]],
	  '3': [[3, 4, 5], [0, 3, 6]],
	  '4': [[3, 4, 5], [0, 4, 8], [2, 4, 6], [1, 4, 7]],
	  '5': [[3, 4, 5], [2, 5, 8]],
	  '6': [[6, 7, 8], [2, 4, 6], [0, 3, 6]],
	  '7': [[6, 7, 8], [1, 4, 7]],
	  '8': [[6, 7, 8], [0, 4, 8], [2, 5, 8]],
	}; 
	quadrados = document.getElementsByClassName('casa');
	let possibilidade = possibilidades[clickedIn];
  
	for( let possi = 0; possi < possibilidade.length; possi++ ) {
	  const [a, b, c] = possibilidade[possi];
  
	  if( quadrados[a].style.backgroundImage === quadrados[b].style.backgroundImage && quadrados[a].style.backgroundImage === quadrados[c].style.backgroundImage && quadrados[a].style.backgroundImage != '') {
		winner = true;
		return jogadores[jogador].nome_do_jogador;
	  };
	};
  
	return null
  };

// para jogar novamente
function jogueDenovo() {
	jogadas = 0;
	jogador = jogador === 0 ? 1 : 0;
	vencedor = false;
  
	document.getElementById('status').innerHTML = `Proximo a jogar: <span id="jogador" class="${jogadores[jogador].nome_do_jogador}">${jogadores[jogador].nome_do_jogador}</span>`;
	document.getElementById('jogar-de-novo').style.display = 'none';
  
	const quadrados = document.getElementsByClassName('casa');
  
	for( let possi = 0; possi < quadrados.length; possi++ ) {
	  quadrados[possi].style.backgroundImage = '';
	};
  };

// para mudar as imagens dos quadrados
function quadradinhos(event) {
	quadrado = event.target;
  
	if( (quadrado.style.backgroundImage === '' || quadrado.style.backgroundImage === null) && vencedor === false) {
	  let caminho = `./img/${jogador}.png`;
	  quadrado.style.backgroundImage = `url('${caminho}')`;
  
	  jogadas++;
	  if( jogadas >= 5 ) {
		let vitoria = checarVitoria(quadrado.id);
  
		if( vitoria != null ) {
		  document.getElementById('status').innerHTML = `Vencedor: <span id="jogador" class="${vitoria}">${vitoria}</span>`
  
		  document.getElementById('jogar-de-novo').style.display = 'inline';
  
		  return;
		};
	  };
  
	  if( jogadas === 9 && vencedor === false ) {
		document.getElementById('status').innerHTML = `Empate!`;
		document.getElementById('jogar-de-novo').style.display = 'inline';
  
		return;
	  };
  
	  jogador = jogadorDaVez(jogador);
	};
  };
  
  